//@author: woei
//@help: greyscale conversion function and saturation adjustment per channel
//@tags: greyscale, saturation, color correction, image adjustment, pixelshader

// PARAMETERS: -----------------------------------------------------------------
//linear coeff {0.3086, 0.6094, 0.0820, 0};
//ntsc (gamma 2.2) coeff = {0.299, 0.587, 0.114, 0};
const float4 coeff = {0.3086, 0.6094, 0.0820, 0};

float3 sat <string uiname="Saturation";> = 1.;

// FUNCTIONS: ------------------------------------------------------------------
float4 Greyscale(float4 Color) {
return float4(dot(Color, coeff).xxx,Color.a);
}
 
float4 Saturation(float4 Color) {
float4 _s = (1.-sat.rgbb)*coeff;

float4x4 mat = float4x4(_s.r+sat.r,_s.r,_s.r,0,
						_s.g,_s.g+sat.g,_s.g,0,
						_s.b,_s.b,_s.b+sat.b,0,
						0,0,0,1);
return mul(Color,mat);
}