//@author: woei
//@help: color correction via (tone)curves
//@tags: tone curve, color correction, image adjustment, pixelshader

// PARAMETERS: -----------------------------------------------------------------
texture Curve <string uiname="Tonecurve";>;
sampler CSamp = sampler_state    
{
    Texture   = (Curve);        
    MipFilter = Linear;       
    MinFilter = Linear;
    MagFilter = Linear;
};

// FUNCTIONS: ------------------------------------------------------------------
float4 Tonecurve(float4 Color) {
    //linear coeff {0.3086, 0.6094, 0.0820, 0};
  	//ntsc (gamma 2.2) coeff = {0.299, 0.587, 0.114, 0};
  	const float4 coeff = {0.3086, 0.6094, 0.0820, 0};
         
    float luma = dot(Color, coeff);
    float4 tone = tex1D(CSamp, luma)-luma;
    return Color+tone;
}