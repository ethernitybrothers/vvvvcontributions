//@author: woei
//@help: Contrast function with overload to adjust the midpoint
//@tags: contrast, color correction, image adjustment, pixelshader

// PARAMETERS: -----------------------------------------------------------------
float3 c <string uiname="Contrast"; float3 uimax={2,2,2};> = {1.,1.,1.};
float3 mid <string uiname="Midpoint";> = {0.5,0.5,0.5};

// FUNCTIONS: ------------------------------------------------------------------
float4 Contrast(float4 Color)
{
	return float4(((Color.rgb - mid) * c) + mid,Color.a); 
}

float4 ContrastMid(float4 Color)
{
	return float4(((Color.rgb - 0.5) * c) + 0.5,Color.a); 
}