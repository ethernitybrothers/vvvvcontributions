//@author: woei
//@help: 'Level' image adjustment function
//@tags: color correction, image adjustment, pixelshader

// PARAMETERS: -----------------------------------------------------------------
float3 bP <string uiname="Black Point";> = 0.;
float3 g <string uiname="Gamma"; float uimin=0.; float uimax=1.;> = 0.5;
float3 wP <string uiname="White Point";> = 1.;

float3 bO<string uiname="Black Offset";> = 0.;
float3 wO<string uiname="White Offset";> = 1.;

// FUNCTIONS: ------------------------------------------------------------------
float4 Levels(float4 Color) {
	  // normalize
    Color.rgb = saturate(lerp(bP,wP, Color.rgb));
    
    //do the mid point
    g = lerp (-10,10,g);
    Color.rgb = pow(Color.rgb, exp2(g));
    
    // get the value back in range
    Color.rgb = lerp(bO,wO,Color.rgb);

    return Color;
}