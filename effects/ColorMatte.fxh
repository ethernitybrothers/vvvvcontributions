//@author: woei
//@help: returns the alpha values of the (keying) matte
//@tags: keying, color keying, pixelshader
//requires sm 2_0

// PARAMETERS: -----------------------------------------------------------------
//linear coeff {0.3086, 0.6094, 0.0820, 0};
//ntsc (gamma 2.2) coeff = {0.299, 0.587, 0.114, 0};
const float4 coeff = {0.3086, 0.6094, 0.0820, 0};

float4 Key : COLOR = {1,1,1,1};
float Bias <float uimin=0;> = 0.1;

// FUNCTIONS: ------------------------------------------------------------------
float ColorMatte(float4 Color) {
	return smoothstep(0.,Bias,abs(distance(Color,Key)));
}

