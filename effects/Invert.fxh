//@author: woei
//@help: Invert Color function with overload to for each channel
//@tags: invert, image adjustment, pixelshader

// PARAMETERS: -----------------------------------------------------------------
bool3 inv <string uiname="Invert";> = {1.,1.,1.};

// FUNCTIONS: ------------------------------------------------------------------
float4 Invert(float4 Color)
{
	return float4(1.-Color.rgb,Color.a);
}

float4 InvertChannels(float4 Color) : Color
{
	Color.rgb = inv ? 1.-Color.rgb : Color.rgb;
	return Color; 
}