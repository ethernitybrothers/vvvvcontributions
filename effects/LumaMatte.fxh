//@author: woei
//@help: returns the alpha values of the (keying) matte
//@tags: keying, luma, inverse luma, pixelshader
//requires sm 2_0

// PARAMETERS: -----------------------------------------------------------------
//linear coeff {0.3086, 0.6094, 0.0820, 0};
//ntsc (gamma 2.2) coeff = {0.299, 0.587, 0.114, 0};
const float4 coeff = {0.3086, 0.6094, 0.0820, 0};

float Level = 0.5;
float Bias <float uimin=0;> = 0.1;

// FUNCTIONS: ------------------------------------------------------------------
float LumaMatte(float4 Color)
{
	float luma = dot(Color, coeff);
	return luma<Level;
}
float LumaMatteBias(float4 Color) 
{
	float luma = dot(Color, coeff);
	return smoothstep(Level-Bias-0.00001,Level,luma);
}

float InvLumaMatte(float4 Color) {
	float luma = dot(Color, coeff);
	return luma>Level;
}
float InvLumaMatteBias(float4 Color)
{
	Level = 1-Level;
	float luma = dot(Color, coeff); 
	return smoothstep(Level+Bias+0.00001,Level,luma);
}

